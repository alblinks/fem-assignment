import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import os

from src.stationary_pde import StationaryPDE
from src.grid import Grid
from src.quadrature import Quadrature
from src.basis import Basis


class InstationaryPDE(StationaryPDE):

    def get_local_indices(self, i, j):
        """Evaluate pairs of DOF for intersecting supports.
        Input:
            i,j [int] - global index of DOF
        Return:
            supp_ij [int] - global index of intersecting support element
            localInd_I - local index of DOF i on support
            localInd_J - local index of DOF j on support

        """
        assert np.isin(i, self.allDOFs), "Point is no valid DOF on the chosen grid"
        assert np.isin(j, self.allDOFs), "Point is no valid DOF on the chosen grid"

        supp_i, localInd_i = self.grid.evalDOFMap(i)
        supp_j, localInd_j = self.grid.evalDOFMap(j)

        supp_ij, int_i, int_j = np.intersect1d(supp_i, supp_j, return_indices=True)
        localInd_i = localInd_i[int_i]
        localInd_j = localInd_j[int_j]
        return supp_ij, localInd_i, localInd_j

    def addMass(self):
        nDOF = np.shape(self.allDOFs)[0]
        self.Mass = np.zeros((nDOF, nDOF))
        for i in self.allDOFs:
            for j in self.allDOFs:
                supp_IJ, localInd_I, localInd_J = self.get_local_indices(i, j)

                for cell, iterator_localInd_I, iterator_localInd_J in zip(supp_IJ, localInd_I, localInd_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Mass[i, j] += self.dets[cell] * self.wkHat[k] * \
                                           np.dot(self.phi[k, iterator_localInd_J], self.phi[k, iterator_localInd_I])

    def l2projection(self, f):
        # Compute the L2 Projection for some function into the FE Space
        lhs = self.Mass
        lhs[self.dirichletDOFs, self.dirichletDOFs] = 1.0

        rhs = np.zeros(np.shape(self.allDOFs)[0])
        rhs[self.dirichletDOFs] = self.Load[self.dirichletDOFs]

        for i in self.freeDOFs:
            supp, localInd = self.grid.evalDOFMap(i)
            for T, loc_i in zip(supp, localInd):
                rhs[i] += self.dets[T] * np.sum(
                    self.phi[:, loc_i] * self.wkHat * f(self.xkTrafo[T, 0, :], self.xkTrafo[T, 1, :]))

        return np.linalg.solve(lhs, rhs)

    def printSolution(self, path):
        if not os.path.exists(path):
            os.mkdir(path)

        for t in range(np.shape(self.solution)[0]):
            fig = plt.figure()
            ax = fig.add_subplot(1, 1, 1, projection='3d')
            ax.set_xlim([np.min(self.grid.points[:, 0]), np.max(self.grid.points[:, 0])])
            ax.set_ylim([np.min(self.grid.points[:, 1]), np.max(self.grid.points[:, 1])])
            ax.set_zlim([np.min(self.solution[0]), np.max(self.solution[0])])

            self.grid.plotDOFVector(self.solution[t], ax)
            plt.savefig(path + '/solution_' + str(t) + '.png')
            plt.close(fig)


class BackwardEuler:
    def __init__(self, pde, initialSolution=(lambda x, y: 0.0), timegrid=np.linspace(0, 1, 10)):
        self.pde = pde

        self.timegrid = timegrid
        self.num_timesteps = np.shape(timegrid)[0] - 1

        self.pde.solution = [np.zeros(np.shape(pde.grid.points)[0]) for t in range(self.num_timesteps + 1)]
        self.pde.solution[0] = self.pde.l2projection(initialSolution)

        self.currentTimeIndex = 1

    def oneStep(self):
        # Timestep
        tau = self.timegrid[self.currentTimeIndex] - self.timegrid[self.currentTimeIndex - 1]
        u_t = self.pde.solution[self.currentTimeIndex - 1]
        rhs = self.pde.Load + np.dot(self.pde.Mass, u_t) / tau  # Assuming load to be time invariant
        A = self.pde.Mass/tau + self.pde.Stiff
        self.pde.solution[self.currentTimeIndex] = np.linalg.solve(A, rhs)


    def allSteps(self):
        # Timestep
        tau = self.timegrid[self.currentTimeIndex] - self.timegrid[self.currentTimeIndex - 1]
        for t in range(len(self.timegrid) - self.currentTimeIndex):  # Iterate over remaining time steps
            self.oneStep()
            self.currentTimeIndex += 1

def main():
    # Create essentials
    grid = Grid(0, 1, 0, 1, 5, 5, True)
    basis = Basis(1)
    quadrature = Quadrature(1, 1)

    # Create the problem
    p = InstationaryPDE(grid, basis, quadrature)
    # Add Laplacian and Mass Matrix
    a = lambda x, y: 0.1
    p.addDiffusion(a)
    p.addMass()

    u0 = lambda x, y: np.exp(- ((x - 0.5) ** 2 + (y - 0.5) ** 2) / 0.1)
    # Create and apply the time discretization scheme to assign p.solution
    e = BackwardEuler(p, u0)
    e.allSteps()

    # Plot the solution into a new directory
    p.printSolution('./output')

if __name__ == '__main__':
    main()
