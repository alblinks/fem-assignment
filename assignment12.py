import numpy as np
import timeit

from src.grid import Grid
from src.basis import Basis
from src.quadrature import Quadrature


class PDE:
    def __init__(self, grid, basis, quadrature, dirichletLocations=(lambda x, y: True),
                 dirichletValues=(lambda x, y: 0.0)):
        self.grid = grid
        self.basis = basis
        self.quadrature = quadrature

        # Initialize
        self.Stiff = np.zeros((np.shape(self.grid.points)[0], np.shape(self.grid.points)[0]))
        self.Load = np.zeros(np.shape(self.grid.points)[0])
        self.solution = None

        # Boundary Conditions
        self.assembleBoundaryConditions(dirichletLocations, dirichletValues)

        # Gather
        self.__gatherData()

    def __gatherData(self):
        " Precompute data that is needed all the time."
        # x_k and w_k
        self.xkHat, self.wkHat = self.quadrature.getPointsAndWeights()
        # F_T(x_k)
        self.xkTrafo = self.grid.evalReferenceMap(self.xkHat)
        # DF_T -> det and inverse
        self.dets = np.abs(self.grid.getDeterminants())
        self.invJac = self.grid.getInverseJacobians()
        # phi(x_k) and grad_phi(x_k)
        self.phi = self.basis.evalPhi(self.xkHat)
        self.gradPhi = self.basis.evalGradPhi(self.xkHat)

    def reset(self):
        self.Stiff = np.zeros((np.shape(self.grid.points)[0], np.shape(self.grid.points)[0]))
        self.Load = np.zeros(np.shape(self.grid.points)[0])
        self.solution = None

    def setGrid(self, grid):
        self.grid = grid
        self.__gatherData()

    def setBasis(self, basis):
        self.basis = basis
        self.__gatherData()

    def setQuadrature(self, quadrature):
        self.quadrature = quadrature
        self.__gatherData()

    def assembleBoundaryConditions(self, dirichletLocations, dirichletValues):
        self.dirichletDOFs = self.grid.getBoundaryIndices(dirichletLocations)
        self.allDOFs = np.arange(np.shape(self.grid.points)[0])
        self.freeDOFs = np.setdiff1d(np.arange(np.shape(self.grid.points)[0]), self.dirichletDOFs)

        self.reset()

        self.Stiff[self.dirichletDOFs, self.dirichletDOFs] = 1.0
        self.Load[self.dirichletDOFs] = dirichletValues(self.grid.points[self.dirichletDOFs, 0],
                                                        self.grid.points[self.dirichletDOFs, 1])
    def get_local_indices(self, i, j):
        """Evaluate pairs of DOF for intersecting supports.
        Input:
            i,j [int] - global index of DOF
        Return:
            supp_ij [int] - global index of intersecting support element
            localInd_I - local index of DOF i on support
            localInd_J - local index of DOF j on support

        """
        assert np.isin(i, self.allDOFs), "Point is no valid DOF on the chosen grid"
        assert np.isin(j, self.allDOFs), "Point is no valid DOF on the chosen grid"

        supp_i, localInd_i = self.grid.evalDOFMap(i)
        supp_j, localInd_j = self.grid.evalDOFMap(j)

        supp_ij, int_i, int_j = np.intersect1d(supp_i, supp_j, return_indices=True)
        localInd_i = localInd_i[int_i]
        localInd_j = localInd_j[int_j]
        return supp_ij, localInd_i, localInd_j

    def addLoad(self, f):
        self.solution = None
        for i in self.freeDOFs:
            supp, localInd = self.grid.evalDOFMap(i)
            for cell, idx in zip(supp, localInd):
                self.Load[i] = self.dets[cell] * np.sum(self.wkHat * self.phi[:, idx]
                                                        * f(self.xkTrafo[cell, 0], self.xkTrafo[cell, 1]))

    def addDiffusion(self, a):
        self.solution = None
        for i in self.freeDOFs:
            for j in self.allDOFs:
                supp_IJ, localInd_I, localInd_J = self.get_local_indices(i, j)

                for cell, iterator_localInd_I, iterator_localInd_J in zip(supp_IJ, localInd_I, localInd_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i, j] += (self.dets[cell] * self.wkHat[k] * np.dot(np.dot(self.invJac[cell, :, :],
                                                             self.gradPhi[k, iterator_localInd_J, :]),
                                                                                      np.dot(self.invJac[cell, :, :],
                                                             self.gradPhi[k, iterator_localInd_I, :]))
                                             * a(self.xkTrafo[cell, 0], self.xkTrafo[cell, 1]))


    def addConvection(self, b):  # Almost the same as diffusion
        self.solution = None
        for i in self.freeDOFs:
            for j in self.allDOFs:
                supp_IJ, localInd_I, localInd_J = self.get_local_indices(i, j)

                for cell, iterator_localInd_I, iterator_localInd_J in zip(supp_IJ, localInd_I, localInd_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i, j] += (self.dets[cell] * self.wkHat[k]
                                             * np.dot(b(self.xkTrafo[cell, 0, :], self.xkTrafo[cell, 1, :]),
                                                      np.dot(self.invJac[cell, :, :],
                                                             self.gradPhi[k, iterator_localInd_J, :]))
                                             * self.phi[k, iterator_localInd_I])

    def addReaction(self, c):
        self.solution = None

        for i in self.freeDOFs:
            for j in self.allDOFs:
                supp_IJ, localInd_I, localInd_J = self.get_local_indices(i, j)

                for cell, iterator_localInd_I, iterator_localInd_J in zip(supp_IJ, localInd_I, localInd_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i, j] += (self.dets[cell] * self.wkHat[k] *
                                             c(self.xkTrafo[cell, 0, :], self.xkTrafo[cell, 1, :])
                                             * self.phi[k, iterator_localInd_J] * self.phi[k, iterator_localInd_I])

    def solve(self):
        self.solution = np.linalg.solve(self.Stiff, self.Load)

    def getSolution(self):
        if self.solution is None:
            self.solve()

        return self.solution

    def plotSolution(self):
        if self.solution is None:
            self.solve()

        self.grid.plotDOFVector(self.solution)

def main():
    grid = Grid(0, 1, 0, 1, 20, 20, True)
    basis = Basis(1)
    quadrature = Quadrature(1, 1)

    # Tests
    f = lambda x, y: 1.0
    a = lambda x, y: 1.0
    b = lambda x, y: np.array([5.0, 0.0])
    c = lambda x, y: 10.0

    p = PDE(grid, basis, quadrature)
    p.addLoad(f)
    p.addDiffusion(a)
    p.addConvection(b)
    p.addReaction(c)
    p.plotSolution()

    dirichletLocation = lambda x, y: x > 0.5
    dirichletValues = lambda x, y: 1.0
    p.assembleBoundaryConditions(dirichletLocation, dirichletValues)
    # Reset to Poissons Equation
    p.addDiffusion(a)
    p.addLoad(f)

    p.plotSolution()

if __name__ == '__main__':
    main()
