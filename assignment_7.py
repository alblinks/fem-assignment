import numpy as np
import warnings


def getGaussQuad(order):
    if order == 1:
        points = 0.5
        weights = 1
    elif order == 2:
        points = np.array([(np.sqrt(3)-1)/(2*np.sqrt(3)), (np.sqrt(3)+1)/(2*np.sqrt(3))])
        weights = np.array([0.5, 0.5])
    elif order == 3:
        points = np.array([(5-np.sqrt(15))/10, 0.5, (5+np.sqrt(15))/10])
        weights = np.array([5/18, 4/9, 5/18])
    else:
        points = np.array([(5-np.sqrt(15))/10, 0.5, (5+np.sqrt(15))/10])
        weights = np.array([5/18, 4/9, 5/18])
        warnings.warn("Requested order of {} could not be achieved. Falling back to order of 3.".format(order))
    return points, weights



def integrateFunction(func, order):
    points, weights = getGaussQuad(order)
    approxInt = np.sum(func(points) * weights)
    return approxInt

def integrateFunctionAB(func,order,a,b):
    assert b >= a, "Integral is only defined for b >= a"
    points, weights = getGaussQuad(order)
    length_of_interval = b - a
    points = a + points * length_of_interval
    approxInt = np.sum(func(points) * weights) * length_of_interval
    return approxInt


def integrateDividedFunctionAB(func,order,a,b,N):
    assert b >= a, "Integral is only defined for b >= a"

    f_xi = np.empty(N)
    for i in range(N):
        a_i = a + (b-a) * i / N
        b_i = a + (b-a) * (i+1) / N
        f_xi[i] = integrateFunctionAB(func, order, a_i, b_i)
    approxInt = np.sum(f_xi)

    return approxInt

if __name__ == '__main__':
    p, w = getGaussQuad(7)
    assert w[2] == 5.0 / 18.0
    #func = lambda x:  x * x * x + np.sin(x)
    #print(integrateFunctionAB(func, 3, -1, 2))
    #print(integrateDividedFunctionAB(func, 1, -1, 2, 100000))

