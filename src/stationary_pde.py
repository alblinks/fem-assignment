"""
    Class representing an abstract PDE problem that can be solved using Finite Element Method.
    Input:
    - Grid
    - Basis 
    - Quadrature 
    - Lambda functions for Location and Value of Dirichlet boundaries

    Properties:
    - Stiff - Stiffness Matrix
    - Load - load vector
    - Solution - after the method solve has been called, solution of the system

    Cached Data - only recomputed if something changes:
    - xkHat,wkHat - Quadrature Points and Weights
    - xkTrafo - Transformed Quadrature Points
    - dets - Determinants of Reference Map Jacobians
    - phi - Values of all basis functions at Quadrature points
"""

import numpy as np
import matplotlib.pyplot as plt

class StationaryPDE:
    def __init__(self, grid, basis, quadrature, dirichletLocations = (lambda x,y: True), dirichletValues = (lambda x,y: 0.0)):
        self.grid = grid
        self.basis = basis
        self.quadrature = quadrature

        # Initialize 
        self.Stiff = np.zeros((np.shape(self.grid.points)[0],np.shape(self.grid.points)[0]))
        self.Load = np.zeros(np.shape(self.grid.points)[0])
        self.solution = None

        # Boundary Conditions
        self.assembleBoundaryConditions(dirichletLocations, dirichletValues)

        # Gather
        self.__gatherData()


    def __gatherData(self):
        " Precompute data that is needed all the time."
        # x_k and w_k
        self.xkHat,self.wkHat = self.quadrature.getPointsAndWeights()
        # F_T(x_k)
        self.xkTrafo = self.grid.evalReferenceMap(self.xkHat)
        # DF_T -> det and inverse
        self.dets = np.abs(self.grid.getDeterminants())
        self.invJac = self.grid.getInverseJacobians()
        # phi(x_k) and grad_phi(x_k)  
        self.phi = self.basis.evalPhi(self.xkHat)
        self.gradPhi = self.basis.evalGradPhi(self.xkHat)
        
    def reset(self):
        self.Stiff[self.dirichletDOFs,:] = 0.0
        self.Load[self.dirichletDOFs] = 0.0
        self.solution = None

    def setGrid(self, grid):
        self.grid = grid
        self.__gatherData()

    def setBasis(self, basis):
        self.basis = basis
        self.__gatherData()

    def setQuadrature(self, quadrature):
        self.quadrature = quadrature
        self.__gatherData()        

    def assembleBoundaryConditions(self, dirichletLocations, dirichletValues):
        self.dirichletDOFs = self.grid.getBoundaryIndices(dirichletLocations)
        self.allDOFs = np.arange(np.shape(self.grid.points)[0])
        self.freeDOFs = np.setdiff1d(np.arange(np.shape(self.grid.points)[0]),self.dirichletDOFs)

        self.reset()

        self.Stiff[self.dirichletDOFs,self.dirichletDOFs] = 1.0
        self.Load[self.dirichletDOFs] = dirichletValues(self.grid.points[self.dirichletDOFs,0],self.grid.points[self.dirichletDOFs,1])

    def addLoad(self, f):
        self.solution = None

        for i in self.freeDOFs:  
            supp,localInd = self.grid.evalDOFMap(i)
            for T,loc_i in zip(supp,localInd):
                self.Load[i] += self.dets[T] * np.sum(self.phi[:,loc_i] * self.wkHat * f(self.xkTrafo[T,0,:],self.xkTrafo[T,1,:]))

    def addDiffusion(self, a):
        self.solution = None

        for i in self.freeDOFs:
            for j in self.allDOFs:        
                supp_I,localIndices_I = self.grid.evalDOFMap(i)
                supp_J,localIndices_J = self.grid.evalDOFMap(j)
                    
                supp_IJ,tmpLocalIndices_I,tmpLocalIndices_J = np.intersect1d(supp_I,supp_J,assume_unique=False,return_indices=True)
                localIndices_I = localIndices_I[tmpLocalIndices_I]
                localIndices_J = localIndices_J[tmpLocalIndices_J]
                
                for T,loc_i,loc_j in zip(supp_IJ,localIndices_I,localIndices_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i,j] += a(self.xkTrafo[T,0,:],self.xkTrafo[T,1,:]) * self.dets[T] * self.wkHat[k] * np.dot(np.dot(self.invJac[T,:,:],self.gradPhi[k,loc_j,:]),np.dot(self.invJac[T,:,:],self.gradPhi[k,loc_i,:]))

    def addConvection(self, b):
        self.solution = None

        for i in self.freeDOFs:
            for j in self.allDOFs:        
                supp_I,localIndices_I = self.grid.evalDOFMap(i)
                supp_J,localIndices_J = self.grid.evalDOFMap(j)
                    
                supp_IJ,tmpLocalIndices_I,tmpLocalIndices_J = np.intersect1d(supp_I,supp_J,assume_unique=False,return_indices=True)
                localIndices_I = localIndices_I[tmpLocalIndices_I]
                localIndices_J = localIndices_J[tmpLocalIndices_J]
                
                for T,loc_i,loc_j in zip(supp_IJ,localIndices_I,localIndices_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i,j] +=  self.dets[T] * self.wkHat[k] * np.dot(b(self.xkTrafo[T,0,:],self.xkTrafo[T,1,:]),np.dot(self.invJac[T,:,:],self.gradPhi[k,loc_j,:])) * self.phi[k,loc_i]

    def addReaction(self, c):
        self.solution = None

        for i in self.freeDOFs:
            for j in self.allDOFs:        
                supp_I,localIndices_I = self.grid.evalDOFMap(i)
                supp_J,localIndices_J = self.grid.evalDOFMap(j)
                        
                supp_IJ,tmpLocalIndices_I,tmpLocalIndices_J = np.intersect1d(supp_I,supp_J,assume_unique=False,return_indices=True)
                localIndices_I = localIndices_I[tmpLocalIndices_I]
                localIndices_J = localIndices_J[tmpLocalIndices_J]
                    
                for T,loc_i,loc_j in zip(supp_IJ,localIndices_I,localIndices_J):
                    for k in range(self.xkHat.shape[0]):
                        self.Stiff[i,j] +=  self.dets[T] * self.wkHat[k] * c(self.xkTrafo[T,0,:],self.xkTrafo[T,1,:]) * self.phi[k,loc_j] * self.phi[k,loc_i]

    def solve(self):
        self.solution = np.linalg.solve(self.Stiff,self.Load)

    def getSolution(self):
        if self.solution is None:
            self.solve()

        return self.solution    

    def plotSolution(self):
        if self.solution is None:
            self.solve()

        self.grid.plotDOFVector(self.solution)            