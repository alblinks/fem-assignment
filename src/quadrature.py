"""
    Quadrature rules in 2D. User can choose a requested order of exactness k. Additionally the user can require the rule to be          designated for triangular shapes. 
    Default setting, i.e. rectangles, measures exactness in terms of the Q polynomials. For rectangles the order concern P polynomials.
"""

import numpy as np

class Quadrature:
    def __init__(self, order, triangles = False):
        if triangles:
            if order == 1:
                self.weights = np.array([0.5])
                self.points = np.array([[1.0/3.0,1.0/3.0]])
            else:
                self.weights = np.array([1.0/6.0,1.0/6.0,1.0/6.0])
                self.points = np.array([[1.0/6.0,1.0/6.0],[2.0/3.0,1.0/6.0],[1.0/6.0,2.0/3.0]])     
                if order > 2: 
                    print("Requested order " + str(order) + " is not implemented. Returning order 2 quadrature.")
        else:
            if order == 1:
                self.weights = np.array([1])
                self.points = np.array([[0.5,0.5]])
            else:
                self.weights = np.array([0.25,0.25,0.25,0.25])
                self.points = np.array([[(np.sqrt(3)-1)/(2*np.sqrt(3)),(np.sqrt(3)-1)/(2*np.sqrt(3))],
                          [(np.sqrt(3)+1)/(2*np.sqrt(3)),(np.sqrt(3)-1)/(2*np.sqrt(3))],
                          [(np.sqrt(3)-1)/(2*np.sqrt(3)),(np.sqrt(3)+1)/(2*np.sqrt(3))],
                          [(np.sqrt(3)+1)/(2*np.sqrt(3)),(np.sqrt(3)+1)/(2*np.sqrt(3))]])
                if order > 2:
                    print("Requested order " + str(order) + " is not implemented. Returning order 2 quadrature.")

    def integrateFunction(self, fun):
        #fun - scalar lambda function defined in 2D, i.e. fun = lambda x,y: ....
        return np.sum(fun(self.points[:,0],self.points[:,1]) * self.weights)    

    def getPoints(self):
        return self.points

    def getWeights(self):
        return self.weights

    def getPointsAndWeights(self):
        return self.points,self.weights
