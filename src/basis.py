"""
	Objects represent a set of linear/bilinear Basis functions over a certain reference domain to be used e.g. with a Finite Element Method. In case of triangular geometry we have 3 linear basis functions, for rectangular geometry there are 4 bilinear basis functions.
	Datastructure:
		- isTri: true if defined on triangles, false if defined on rectangles
    Functions:
        - evalPhi: evaluate all basis functions at a set of points with shape Np x 2; returns Np x (3/4)
        - evalGradPhi: evaluate the gradients of all basis functions at a set of points with shape Np x 2; returns Np x (3/4) x 2

"""

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

class Basis:
    def __init__(self, triangles = False):
        self.isTri = triangles

    def evalPhi(self, xHat):
        Np = np.shape(xHat)[0]

        if self.isTri:
            phi = np.zeros((Np,3))
            phi[:,0] = 1 - xHat[:,0] - xHat[:,1]
            phi[:,1] = xHat[:,0]
            phi[:,2] = xHat[:,1]

        else:  
            phi = np.zeros((Np,4))
            phi[:,0] = (1 - xHat[:,0]) * (1 - xHat[:,1])
            phi[:,1] = (1 - xHat[:,1]) * xHat[:,0]
            phi[:,2] = xHat[:,1] * (1 - xHat[:,0])
            phi[:,3] = xHat[:,0] * xHat[:,1]
    
        return phi

    def evalGradPhi(self, xHat):
        Np = np.shape(xHat)[0]

        if self.isTri:
            gradPhi = np.zeros((Np,3,2))

            gradPhi[:,0,0] = -1
            gradPhi[:,0,1] = -1
            
            gradPhi[:,1,0] = 1  
            gradPhi[:,1,1] = 0

            gradPhi[:,2,0] = 0  
            gradPhi[:,2,1] = 1   

        else:
            gradPhi = np.zeros((Np,4,2))
            
            gradPhi[:,0,0] = xHat[:,1] - 1
            gradPhi[:,0,1] = xHat[:,0] - 1

            gradPhi[:,1,0] = 1 - xHat[:,1] 
            gradPhi[:,1,1] = - xHat[:,0]

            gradPhi[:,2,0] = - xHat[:,1] 
            gradPhi[:,2,1] = 1 - xHat[:,0]

            gradPhi[:,3,0] = xHat[:,1] 
            gradPhi[:,3,1] = xHat[:,0]

        return gradPhi

    def plotPhi(self, index):
        
        fig = plt.figure()
        ax = fig.add_subplot(1,1,1,projection='3d')
    
        x = np.linspace(0,1,50)
        X,Y = np.meshgrid(x,x)
    
        X = np.reshape(X,(50**2,1))
        Y = np.reshape(Y,(50**2,1))
    
        xHat = np.stack((X,Y),axis=1)
        xHat = np.squeeze(xHat)
        if self.isTri:
            xHat[xHat[:,0]+xHat[:,1]>1,:] = float('nan')
    
        phi = self.evalPhi(xHat)
    
        X = np.reshape(X,(50,50))
        Y = np.reshape(Y,(50,50))
        phi = np.reshape(phi[:,index],(50,50))   
    
        ax.plot_surface(X,Y,phi)
        plt.show()
