import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

def generateGrid(a,b,N):
    e = np.linspace(a, b, N)
    N_square = N ** 2
    mesh_grid = np.meshgrid(e, e)
    x_components = mesh_grid[0].reshape(N_square,)
    y_components = mesh_grid[1].reshape(N_square,)
    grid = np.stack([x_components, y_components])
    grid = grid.T  # As required for solution, but numpy uses row-major ordering (as C)
    assert grid.shape == (N_square, 2)
    assert isinstance(grid, np.ndarray)
    return grid


def assembleSystem_example(grid, f):
    ### BEGIN SOLUTION
    N = int(np.sqrt(np.shape(grid)[0]))
    h = grid[1, 0] - grid[0, 0]
    A = np.identity(N ** 2)
    l = np.zeros(N ** 2)
    for i in range(N ** 2):
        if (i < N) or (i % N == 0) or ((i + 1) % N == 0) or (i > (N - 1) * N):
            continue
        for j in range(N ** 2):
            A[i, i] = 4 / h ** 2
            A[i, i - 1] = -1 / h ** 2
            A[i, i + 1] = -1 / h ** 2
            A[i, i - N] = -1 / h ** 2
            A[i, i + N] = -1 / h ** 2

        l[i] = f(grid[i, 0], grid[i, 1])

    ### END SOLUTION
    return A, l

def assembleSystem(grid, f):
    # YOUR CODE HERE
    b = np.empty(len(grid))
    A = np.zeros([len(grid), len(grid)])
    N = np.sqrt(len(grid))
    h = 1/(N-1)


    for k in range(len(grid)):  # Iterate over line/column
        i = grid[k, 0]
        j = grid[k, 1]
        if boundary_check(i, j):  # Point is element of the boundary
            if np.isclose(i, j):
                A[k, k] = 1.0
            else:
                A[k, k] = 0.0  # Not necessary, just for completeness
            b[k] = 0.0
        else:
            A[k, k] = np.divide(4, h**2)
            A[k, k-1] = np.divide(-1.0, h**2)
            A[k, k+1] = np.divide(-1.0, h**2)
            A[k, int(k-(N-2))] = np.divide(-1.0, h**2)
            A[k, int(k+(N-2))] = np.divide(-1.0, h**2)
            b[k] = f(i, j)

    return A, b

def boundary_check(x, y):
    if np.isclose(x, 0) or np.isclose(x, 1):
        return True
    elif np.isclose(y, 0) or np.isclose(y, 1):
        return True
    else:
        return False

def visualizeDOFVector(grid, u):
    u = np.reshape(u, (100,1))
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(grid[:, 0], grid[:, 1], u, cmap=cm.coolwarm)
    plt.show()

def visualizeDOFVector2(grid, u):
    u = np.reshape(u, (100,1))
    X = np.reshape(grid[:, 0], [10, 10])
    Y = np.reshape(grid[:, 1], [10, 10])
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    ax.scatter(grid[:, 0], grid[:, 1], u, cmap=cm.coolwarm)
    plt.show()

def main():
    # boundaries
    a = 0
    b = 1
    # grid points
    N = 10
    # right-hand side
    f = lambda x, y: y * y

    grid = generateGrid(a, b, N)

    B, q = assembleSystem(grid, f)
    A, u = assembleSystem_example(grid, f)
    visualizeDOFVector2(grid, u)

if __name__ == '__main__':
    main()
