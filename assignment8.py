import numpy as np
import warnings

def biquadratic():
    points = np.array([[(np.sqrt(3) - 1) / (2 * np.sqrt(3)), (np.sqrt(3) - 1) / (2 * np.sqrt(3))],
                       [(np.sqrt(3) + 1) / (2 * np.sqrt(3)), (np.sqrt(3) - 1) / (2 * np.sqrt(3))],
                       [(np.sqrt(3) - 1) / (2 * np.sqrt(3)), (np.sqrt(3) + 1) / (2 * np.sqrt(3))],
                       [(np.sqrt(3) + 1) / (2 * np.sqrt(3)), (np.sqrt(3) + 1) / (2 * np.sqrt(3))]])
    weights = np.array([0.25, 0.25, 0.25, 0.25])
    return points, weights

def getRectQuad(order):
    if order == 1:
        points = np.array([0.5, 0.5])
        weights = np.array([1])
    elif order == 2:
        points, weights = biquadratic()
    else:
        points, weights = biquadratic()
        warnings.warn("Requested order of {} could not be achieved. Falling back to order of 3.".format(order))
    return points, weights


def integrateFunctionQuad(func, order):
    points, weights = getRectQuad(order)
    if order == 1:
        approxInt = np.sum(func(points[0], points[1]) * weights)
    else:
        approxInt = np.sum(func(points[:, 0], points[:, 1]) * weights)
    return approxInt

def biquadratic_tri():
    points = np.array([[1/6, 1/6],
                       [2/3, 1/6],
                       [1/6, 2/3]])
    weights = np.array([1/6, 1/6, 1/6])
    return points, weights

def getTriQuad(order):
    if order == 1:
        points = np.array([1/3, 1/3])
        weights = np.array([0.5])
    elif order == 2:
        points, weights = biquadratic_tri()
    else:
        points, weights = biquadratic_tri()
        warnings.warn("Requested order of {} could not be achieved. Falling back to order of 2.".format(order))
    return points,weights

def integrateFunctionTri(func, order):
    points, weights = getTriQuad(order)
    if order == 1:
        approxInt = np.sum(func(points[0], points[1]) * weights)
    else:
        approxInt = np.sum(func(points[:, 0], points[:, 1]) * weights)
    return approxInt

if __name__ == '__main__':
    f_x = lambda x, y: x + y
    integral = integrateFunctionQuad(f_x, 2)
    integral_tri = integrateFunctionTri(f_x, 2)
    print(integral, integral_tri)
